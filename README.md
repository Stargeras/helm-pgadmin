# Pull down chart from oci registry
```bash
helm registry login registry.gitlab.com
helm pull oci://registry.gitlab.com/stargeras/helm-pgadmin/pgadmin --version=1.0.0
```

# Sample terraform code to deploy from OCI registry
```terraform
provider "helm" {
  registry {
    url      = "oci://registry.gitlab.com"
    username = "username"
    password = "password"
  }
}

resource "helm_release" "pgadmin" {
  name             = "pgadmin"
  namespace        = "pgadmin"
  repository       = "oci://registry.gitlab.com"
  version          = "1.0.0"
  chart            = "stargeras/helm-pgadmin/pgadmin"
  create_namespace = true
}
```


